
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "Level02.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	//setting level to null
	m_pLevel = nullptr;
	//Switching levels
	switch (levelIndex)
	{
		//Switching to level 1
	case 0: m_pLevel = new Level01(); break;
		//Level 2
	case 1: m_pLevel = new Level02(); break;
	}

	//Transition fade into level
	SetTransitionInTime(1.0f);
	//Fade out of level
	SetTransitionOutTime(0.5f);
	//Show level
	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
