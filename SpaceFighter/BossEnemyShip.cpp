
#include "BossEnemyShip.h"


BossEnemyShip::BossEnemyShip()
{
	SetSpeed(200);
	SetMaxHitPoints(5);
	SetCollisionRadius(25);
}


void BossEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BossEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Red, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
